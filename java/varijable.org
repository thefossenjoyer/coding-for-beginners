#+TITLE: Varijable

* Sta su varijable?
Moramo i ovo da predjemo.

Varijable, odnosno promenljive su rezervisani delovi memorije u kojima mozemo skladistiti podatke.

#+begin_src java

class varijable{
    public static void main(String[] args){
        String ime = "Pacijent Dovla";
        int godine = 69;
    }
}
#+end_src

* Tipovi podataka(data types)

Ne samo u Javi, vec u svim programskim jezicima postoje razni tipovi podataka

** Brojevni tipovi
Integer(int) - ceo broj
Float(float) - decimalni broj do 8 decimala
Double(double) - -||- do 16 decimala
Bool(bool) - tacno/netacno

** String tipovi
Char(char) - karakter
String(String) - niz karaktera
