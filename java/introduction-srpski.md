# Uvod u Java
AUTHOR: x86oranges
DESCRIPTION: Znam da je vecini Java poznata, ali mora da se pocne od nekud

# Sta je Java?
Java je general-purpose, statically-typed programski jezik.

- general-purpose - moze se prakticno za sve koristiti
- statically-typed - data tip varijable se mora napisati

## Kako bi jedan Java program izgledao?
```java
class Testing{
    public static void main(String[] args){
        int age = 69;

        System.out.println("hi mom");
    }
}
```

# Zasto bi neko ucio Javu?
Java je i dalje jako trazen jezik.
Plus, Java se koristi vec dugo, znaci dosta resursa.

